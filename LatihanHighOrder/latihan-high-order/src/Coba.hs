module Coba where

    data Direction = North | East | South | West
                  deriving (Eq,Show,Enum)

    right :: Direction -> Direction
    right d = toEnum (succ (fromEnum d) `mod` 4)

    data Rank = R1 | R2 deriving(Show)

    newtype Test = Test Int

    test a b= let 
                x = a+b
                y = b+b
                in
                    x+y