module FindKPK where

findKPK:: Int -> Int -> Int
findKPK a b = [x| x <- [1..(a*b)] , x `mod ` a == 0 && x `mod` b ==0] !! 0