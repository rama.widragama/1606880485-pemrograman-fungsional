module Uts
    (
    ) where

import Data.List 
--this function is used for counting length of a list using map and sum 
panjangList :: [a] -> Integer
panjangList [] = 0
panjangList (x:xs)= 1 + panjangList xs

-- what does (+1)(map(+1)xs) do?
--this function is running a ffunction where map f(map g xs) where f and g are arbitary function.. 
--so in general this function run map g xs and the result of that is a list and we assume that result is p and then run the map f(p)
function1 xs = map (+1) (map (+1) xs)

--Give the type of, and define the function iter so that • iter n f x = f (f (... (f x))) 
--where f occurs n times on the right-hand side of the equation. For instance, we should have 
iter :: Int -> (a -> a) -> a -> a
iter 0 f x = x
iter n f x= f ( iter (n-1) f x ) 

--How would you define the sum of the squares of the natural numbers 1 to n using map and foldr? 
sumSquares list n= foldr (\x y -> x**2+y) 0 [1..n] 

--this function i used to copy list from argument xs and copy to array fold [] append it from the left of array
mystery xs = foldr (++) [1,2,3] (map sing xs) 
    where 
        sing x = [x+1]

--If id is the polymorphic identity function, defined by id x = x, explain the behavior of the expressions • (id . f)        (f . id)        (id f) 
{-
(id . f)  is the same as f, because the result of f is fed to the
          identity function id, and thus not changed
(id :: Bool -> Bool)

(f . id)  is the same as f, because the argument of f is fed to the
          identity function id, and thus not changed
(id :: Int -> Int)

id f      is the same as f, because applying id to something does not
          change it
(id :: (Int -> Bool) -> (Int -> Bool))
-}

--Define a function composeList which composes a list of functions into a single function.
--You should give the type of composeList, 
--and explain why the function has this type. 
--What is the effect of your function on an empty list of functions? • (*) Define the function 
composeList :: [a -> a] -> (a -> a)
composeList = foldr (.) id 

--(*) Define the function
flipz :: (a -> b -> c) -> (b -> a -> c)
flipz f n m = f m n

--Can you rewrite the following list comprehensions using the higher-order functions map and filter? 
--You might need the function concat too.
no1 xs = map (+1) xs
no2 xs ys =concat(map (\x -> map(\y ->x+y) ys) xs)
no3 xs = map (+2) (filter (>3) xs)
no4 xys = map ((+3) . fst) xys
no5 xys = map ((+4) . fst) (filter (\(x,y)->x+y<5) xys)
-- no6 mxs = map (\(Just x) -> x+5) (filter isJust mxs)
            

--Can you it the other way around? 
--I.e. rewrite the following expressions as list comprehensions. 
no1_b xs = [ x+3 | x <- xs ]  
no2_b xs = [ x | x <- xs , x>7]
no3_b xs ys = [(x,y)| x<- xs,y<-ys]
no4_b xys = [ x+y | (x,y) <- xys, x+y > 3 ]

-- quickSort []      = []
-- quickSort [x]     = [x]
-- quickSort (x:xs)  = quickSort [y | y <- xs, y<=x] ++ [x] ++ quickSort [y | y <- xs, y>x]

perms [] = [[]]
perms ls = [ x:ps | x <- ls, ps <- perms (ls\\[x]) ] 


lengthList xs= sum(map (\x -> 1) xs)

iter2 :: Int -> (a->a) -> a -> a
iter2 0 f = id
iter2 x f= f . iter2 (x-1) f

sumPangkat n = foldr (+) 0 $ map (**2) [1..n] 

mysteryzs xs = concat (foldr (++) [4,5,6] (map sing xs) where sing x = [x] )


hiphip xs ys= map (\x -> map (\y -> x+y ) ys) xs



