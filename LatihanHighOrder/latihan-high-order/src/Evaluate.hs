module Evaluate where


    data Expr = C Float 
            | Expr :+ Expr 
            | Expr :- Expr
            | Expr :* Expr 
            | Expr :/ Expr  

    foldExp (add , mult,sub,div) (C x) = x
    foldExp (add, mult, sub, div) (x :+ y) = add (foldExp (add, mult, sub, div) x) (foldExp (add, mult, sub, div) y)
    foldExp (add, mult, sub, div) (x :- y) = mult (foldExp (add, mult, sub, div) x) (foldExp (add, mult, sub, div) y)
    foldExp (add, mult, sub, div) (x :* y) = sub (foldExp (add, mult, sub, div) x) (foldExp (add, mult, sub, div) y)
    foldExp (add, mult, sub, div) (x :/ y) = div (foldExp (add, mult, sub, div) x) (foldExp (add, mult, sub, div) y)

    newEval expr= foldExp (add, mult, sub, div) expr
          where add a b= a+b
                mult a b= a*b
                sub a b= a - b
                div a b=  a `div` b
    

